import {useEffect, useState} from "react";
import Posts from "./components/Posts/Posts";
import Form from "./components/Form/Form";
const url = 'http://146.185.154.90:8000/messages';

const App = () => {
    const [author, setAuthor] = useState('');
    const [userMessage, setUserMessage] = useState('');
    const [posts, setPosts] = useState([]);

    const sendMessage = async e => {
        e.preventDefault();

        const data = new URLSearchParams();
        data.set('author', author);
        data.set('message', userMessage);

        await fetch(url, {
            method: 'POST',
            body: data,
        });
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const post = await response.json();
                setPosts(post);
            }
        };

        fetchData().catch(e => console.log(e));
    }, [])

    useEffect(() => {
        let lastDateTime = null;
        if (posts.length > 0) {
            lastDateTime = posts[posts.length - 1].datetime;
        }
        setInterval(async () => {
            const response = await fetch(url + '?datetime=' + lastDateTime);

            if (response.ok && posts.length > 0) {
                const post = await response.json();

                if (post.length > 0) {
                    post.forEach(value => {
                        setPosts([...posts, {author: value.author, message: value.message, datetime: value.datetime}])
                    })
                }
            }
        }, 3000)
    }, [posts])

    return (
        <>
            <Form
                author={author}
                setAuthor={setAuthor}
                userMessage={userMessage} sendMessage={sendMessage}
                setUserMessage={setUserMessage}
            />
            {posts.map((post, i) => {
                return (
                    <Posts key={i} post={post}/>
                )
            })}
        </>
    )
};

export default App;
