import React from 'react';
import './Form.css';

const Form = ({sendMessage, author, setAuthor, userMessage, setUserMessage}) => {
    return (
        <form className="form" onSubmit={sendMessage}>
            <input type="text" value={author} onChange={e => setAuthor(e.target.value)}/>
            <textarea cols="40" rows="15" value={userMessage} onChange={e => setUserMessage(e.target.value)}/>
            <button type="submit">Отправить</button>
        </form>
    );
};

export default Form;