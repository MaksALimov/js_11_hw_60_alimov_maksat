import React from 'react';
import './Posts.css'

const Posts = ({post}) => {
    return (
        <div className="wrapper">
            <p>Author: {post.author}</p>
            <p>Message: {post.message}</p>
            <p>DateTime: {post.datetime}</p>
        </div>
    );
};

export default Posts;